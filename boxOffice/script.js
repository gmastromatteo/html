const app = document.getElementById('root');

const logo = document.createElement('img');
logo.src = 'logo.png';

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);

var request = new XMLHttpRequest();
request.open('GET', 'movies.json', true);
request.onload = function () {

 
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    data.forEach(movie => {
      const card = document.createElement('div');
      card.setAttribute('class', 'card');
      // card.attr('class','card');
      // card.addClass("card");

      var img = document.createElement('img');    
      img.src = movie.image; 
      
      const h1 = document.createElement('h1');
      h1.textContent = movie.title;

      const p = document.createElement('p');
      movie.description = movie.description.substring(0, 300);
      p.textContent = `${movie.description}...`;

      const h2 = document.createElement('h2');
      movie.profits = movie.profits.substring(0, 300);
      h2.textContent = "Incasso al botteghino: $" + `${movie.profits}`;

      container.appendChild(card);
      card.appendChild(img);
      card.appendChild(h1);
      card.appendChild(p);
      card.appendChild(h2);
    });
  } else {
    const errorMessage = document.createElement('marquee');
    errorMessage.textContent = `Not working!`;
    app.appendChild(errorMessage);
  }
}

request.send();