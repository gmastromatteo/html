const form = document.querySelector('form');
const ul = document.querySelector('ul');
const button = document.querySelector('button');
const input = document.getElementById('item');
let elencoOggetti = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

localStorage.setItem('items', JSON.stringify(elencoOggetti));
const data = JSON.parse(localStorage.getItem('items'));

const liElenco = (text) => {
  const li = document.createElement('li');
  li.textContent = text;
  ul.appendChild(li);
}

form.addEventListener('submit', function (e) {
  e.preventDefault();

  elencoOggetti.push(input.value);
  localStorage.setItem('items', JSON.stringify(elencoOggetti));
  liElenco(input.value);
  input.value = "";
});

data.forEach(item => {
  liElenco(item);
});

button.addEventListener('click', function () {
  localStorage.clear();
//   ul.removeChild(ul.firstChild); /* eliminazione FIFO */
  while (ul.firstChild) {
    ul.removeChild(ul.firstChild);
  }
  elencoOggetti = [];
});