const container = document.querySelector('.container');
// grab all the seats in the row that are not occupied
const seats = document.querySelectorAll('.row .seat:not(.occupied)');
const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');

populateUI();

let ticketPrice = +movieSelect.value;

// Save selected movie index and price
function setMovieData(movieIndex, moviePrice) {
  localStorage.setItem('selectedMovieIndex', movieIndex);
  localStorage.setItem('selectedMoviePrice', moviePrice);
}

// Update total and count
function updateSelectedCount() {
  const selectedSeats = document.querySelectorAll('.row .seat.selected');

  const seatsIndex = [...selectedSeats].map((seat) => [...seats].indexOf(seat));

  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

  const selectedSeatsCount = selectedSeats.length;

  count.innerText = selectedSeatsCount;
  total.innerText = selectedSeatsCount * ticketPrice;
}

// Get data from localstorage and populate UI
function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));

  if (selectedSeats !== null && selectedSeats.length > 0) {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1) {
        seat.classList.add('selected');
      }
    });
  }

  const selectedMovieIndex = localStorage.getItem('selectedMovieIndex');

  if (selectedMovieIndex !== null) {
    movieSelect.selectedIndex = selectedMovieIndex;
  }
}

// Movie select event
movieSelect.addEventListener('change', (e) => {
  ticketPrice = +e.target.value;
  setMovieData(e.target.selectedIndex, e.target.value);
  updateSelectedCount();
});

// Seat click event
container.addEventListener('click', (e) => {
  if (
    e.target.classList.contains('seat') &&
    !e.target.classList.contains('occupied')
  ) {
    e.target.classList.toggle('selected');

    updateSelectedCount();
  }
});

// Initial count and total set
updateSelectedCount();


$('select').on('load change', function() {
  if(document.getElementById('movie').value == "12") {
    $('.screen').css("background-image", "url('https://www.fumettologica.it/wp-content/uploads/2021/12/the-batman-670x377.jpg'");
  } else if(document.getElementById('movie').value == "10") {
    $('.screen').css("background-image", "url('https://static.fanpage.it/wp-content/uploads/sites/33/2021/12/lady-gaga-house-of-gucci.jpg'");
  } else if(document.getElementById('movie').value == "5") {
    $('.screen').css("background-image", "url('https://pad.mymovies.it/filmclub/2022/02/231/coverlg.jpg'");
  } else if(document.getElementById('movie').value == "7") {
    $('.screen').css("background-image", "url('https://staticfanpage.akamaized.net/wp-content/uploads/sites/33/2021/12/matrix-resurrection-location-dove-stato-girato.jpeg'");
  }
});

