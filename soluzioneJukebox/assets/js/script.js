window.onload = function(){
  document.getElementById("insert").addEventListener("click", AddToList); // Add to list
  document.getElementById("delete").addEventListener("click", DeleteFromList);
  document.getElementById("view").addEventListener("click", ShowList); //Show the list
  document.getElementById("home").addEventListener("click", back);


  if(localStorage.getItem("idDati") == null){
    localStorage.setItem("idDati", "0");
  }
};


// Metodo per aggiungere elementi a localStorage
// Aggiungi alla lista
function AddToList(){

  if(ObjectReceive() != false){
    // L'immissione di una stringa di dati JSON come localStorage memorizza solo la chiave: valore in formato stringa
    // ObjectReceive(); - Crea un oggetto
    var stringJSON = ObjectReceive();

    // Variabile contatore ID
    // idDati
    var idbr = localStorage.getItem("idDati");

    localStorage.setItem(idbr, stringJSON);

    // Dobbiamo convertirli in un numero per aumentarli. 
    // In caso contrario, viene trattata come una stringa, quindi vengono trovati solo i numeri
    // converti in numero
    idbr = Number(idbr) + 1;
    
    // idDati
    localStorage.setItem("idDati", idbr);


    document.getElementById("songName").value = "";
    document.getElementById("authorName").value = "";
    document.getElementById("year").value = "";

    document.getElementById("done").style.display = "block";  
  }
  window.setTimeout(closePopUp, 2500);

}

// Metodo per creare un oggetto JSON, restituisce questo oggetto se la voce è corretta
function ObjectReceive(){

  var song;
  var singer;
  var year;

  song = document.getElementById("songName").value;
  singer = document.getElementById("authorName").value;
  year = document.getElementById("year").value;

  if(song == "" || singer == "" || year == "")
  { // Attenzione tutti i campi devono essere compilati
    document.getElementById("warning").style.display = "block";
    return false;
  }

  var item = {
    "authorName": singer,
    "naziv": song,
    "year": year
  };
  item = JSON.stringify(item);

  return item;
}

// Pulisci tutto localStorage
// DeleteFromList
function DeleteFromList(){
  document.getElementById("deleteAll").style.display = "block";
  localStorage.clear();
  localStorage.setItem("idDati", "0");
  window.setTimeout(closePopUp, 2500);
}

// Apri sezione lista
function ShowList(){
 document.getElementById("tab").style.display = "none";
    document.getElementById("tab2").style.display = "block";
  goHome();
  TableView();
}

// Metodo per controllare gli avvisi popup sullo stato dell'input
function closePopUp(){
  document.getElementById("warning").style.display = "none";
  document.getElementById("done").style.display = "none";
  document.getElementById("deleteAll").style.display = "none";
}

// Indietro
function back() {
    document.getElementById("tab").style.display = "block";
    document.getElementById("tab2").style.display = "none";
}

// Metodo per tabella principale
function goHome() {
    var k = 0;

    // elimina l'intera tabella
    var board = document.getElementById("board");
    board.innerHTML = "";

    var tableH = document.createElement("thead");
    var row = document.createElement("tr");

    for (k = 0; k < 4; k++) {
        var header = document.createElement("th");
        var text;
        if (k == 0) {
            text = document.createTextNode("Cantante");
            header.appendChild(text);
        }
        if (k == 1) {
            text = document.createTextNode("Canzone");
            header.appendChild(text);
        }
        if (k == 2) {
            text = document.createTextNode("Anno");
            header.appendChild(text);
        }
        if (k == 3) {
            text = document.createTextNode("Elimina");
            header.appendChild(text);
        }

        row.appendChild(header);
    }

    tableH.appendChild(row);
    board.appendChild(tableH);
}

// TableView
function TableView() {
    // Variabili per scorrere localStorage
    var i = 0;
    var l = localStorage.length;

    // Variabili per ottenere dati da JSON
    var p;
    var pj;
    var g;

    // Variabile per creare un pulsante di eliminazione in ogni riga
    var b;

    var board = document.getElementById("board");

    var tableB = document.createElement("tbody");
    //selten;
    var redci;

    // Variabile in cui prepareremo la stringa da localStorage e su cui lavoreremo in formato JSON
    // jsonDaten
    var jsonDaten;

    for (i = 0; i < l; i++) {
        redci = document.createElement("tr");

        key = localStorage.key(i);

        if(key != "idDati"){
            pod = localStorage.getItem(key);
            jsonDaten = JSON.parse(pod);

            p = jsonDaten.authorName;
            pj = jsonDaten.naziv;
            g = jsonDaten.year;

            for (var j = 0; j < 4; j++) {
                stupac = document.createElement("td");
                var text;
                if (j == 0) {
                    text = document.createTextNode(p);
                    stupac.appendChild(text);
                }
                if (j == 1) {
                    text = document.createTextNode(pj);
                  
                    stupac.setAttribute("id", i);
                    stupac.setAttribute("style", "color:blue");
                    stupac.setAttribute("data-toggle", "modal");
                    stupac.setAttribute("data-target", "#myModal");
                    stupac.addEventListener("click", PokreniVideo);
                    stupac.appendChild(text);
                }
                if (j == 2) {
                    text = document.createTextNode(g);
                    stupac.appendChild(text);
                }
                if (j == 3) {
                    stupac.classList.toggle("btn");
                    stupac.classList.toggle("btn-danger");
                    stupac.style.width = "100%";
                    stupac.innerHTML = '<i class="fa fa-trash-o" style="color:black font-size:1vw" aria-hidden="true"></i>';
                    stupac.setAttribute("id", i);
                    stupac.addEventListener("click", DeleteRecord);
                    /*stupac.appendChild(b);*/
                }
                redci.appendChild(stupac);
            }

            tableB.appendChild(redci);
            board.appendChild(tableB);

        }     
    }
}

// elimina riga
function DeleteRecord() {
    document.getElementById("done2").style.display = "block";
    var key = localStorage.key(this.id);
    localStorage.removeItem(key);
    Home();
    TableView();
    setTimeout(closePopUp2, 2500);
}

function closePopUp2() {
    document.getElementById("done2").style.display = "none";
}

function PokreniVideo(){
    var key = localStorage.key(this.id);
    
    var data = JSON.parse(localStorage.getItem(key));
   
    var player = document.createElement("embed");
      $("embed").attr("id","iplayer");
      $("embed").css(
      {"width": "100%",
      "height": "360px"});
  
    $(".modal-body").append(player);
  

    var song = data.naziv;
    var singer = data.authorName;

    var urlVideo = "https://www.youtube.com/v/";

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(xhttp.readyState == 4 && xhttp.status == 200){
            var odgovor = JSON.parse(xhttp.responseText);
            urlVideo += odgovor.items[0].id.videoId;
            document.getElementById("modalHeader").innerHTML = singer + " - " + song;
            document.getElementById("iplayer").src = urlVideo;
          
document.getElementById("closeModal").addEventListener("click", songRemove);
        }
    };
    var urlRequest = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" + song +
        "-" + singer + "&maxResults=2&key=AIzaSyChr6PFx7xgvuleKHPtdE2yvXgEuc5e8Tc";
    xhttp.open("GET",urlRequest,false);
    xhttp.send();
}
// elimina canzone
function songRemove(){
  document.getElementById("iplayer").remove();
}

