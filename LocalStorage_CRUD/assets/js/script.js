$(function () {
    var operation = "C"; //"C"=Creazione
    var selected_index = -1; // Indice dell'elemento selezionato nell'elenco
    var tblPersons = localStorage.getItem("tblPersons"); // Restituisci dati memorizzati
    tblPersons = JSON.parse(tblPersons); // Converti stringa in oggetto
    if (tblPersons === null) // Se non ci sono dati, determina array vuoto
        tblPersons = [];
  
    function Create() {
      // Ottieni valori inseriti nell'html e convertili in stringa
      var person = JSON.stringify({
          Nome: $("#Nome").val(),
          CF: $("#CF").val(),
          Telefono: $("#Telefono").val(),
          Email: $("#Email").val(),
          Comune: $("#Comune").val(),
          Regione: $("#Regione").val()
      }); 
      // Inserimento oggetto in tabella
      tblPersons.push(person);
      // Archiviazione dati in LocalStorage
      localStorage.setItem("tblPersons", JSON.stringify(tblPersons));
      alert("Dati memorizzati"); // Messaggio di avviso
      return true;
    }
  
    function Edit() {
      // Modifica l'elemento selezionato nella tabella
      tblPersons[selected_index] = JSON.stringify({
          Nome: $("#Nome").val(),
          CF: $("#CF").val(),
          Telefono: $("#Telefono").val(),
          Email: $("#Email").val(),
          Comune: $("#Comune").val(),
          Regione: $("#Regione").val()
      });
      // Conserva gli oggetti in localStorage
      localStorage.setItem("tblPersons", JSON.stringify(tblPersons)); 
      alert("I dati sono stati modificati"); // Messaggio di avviso
      return true;
    }
  
    function Delete() {
      // Elimina l'elemento selezionato nella tabella
      tblPersons.splice(selected_index, 1); 
      // Aggiorna i dati di LocalStorage
      localStorage.setItem("tblPersons", JSON.stringify(tblPersons)); 
      alert("Record eliminato"); // Messaggio di avviso
    }
  
    function List() {
      $("#tblList").html("");
      $("#tblList").html(
              "<thead>" +
              "<tr>" +                
              "<th>Nome</th>" +
              "<th>CF</th>" +
              "<th>Telefono</th>" +
              "<th>Email</th>" +
              "<th>Comune</th>" +
              "<th>Regione</th>" +
              "<th>Azioni</th>" +
              "</tr>" +
              "</thead>" +
              "<tbody>" +
              "</tbody>"
              ); // Aggiungi tabella a struttura HTML
      for (var i in tblPersons) {
          var per = JSON.parse(tblPersons[i]);
          $("#tblList tbody").append("<tr>" +                    
                  "<td>" + per.Nome + "</td>" +
                  "<td>" + per.CF + "</td>" +
                  "<td>" + per.Telefono + "</td>" +
                  "<td>" + per.Email + "</td>" +
                  "<td>" + per.Comune + "</td>" + 
                  "<td>" + per.Regione + "</td>" +                  
                                     
                   "<td><img src='http://res.cloudinary.com/demeloweb/image/upload/v1497537879/edit_n51oto.png' alt='Edit" + i + "' class='btnEdit'/>&nbsp &nbsp<img src='http://res.cloudinary.com/demeloweb/image/upload/v1497537882/delete_ntuxjl.png' alt='Delete" + i + "' class='btnDelete'/></td>" +                  
                  
                  "</tr>"
                  );
      } // Carica e inserisci gli articoli nella tabella
    }
  
    $("#frmPerson").bind("submit", function () {
      if (operation === "C")
          return Create();
      else
          return Edit();
    }); 
    
    List();
  
    $(".btnEdit").bind("click", function () {
      operation = "E"; // "E" = Edita
      // Ottieni l'identificatore dell'elemento da modificare
      selected_index = parseInt($(this).attr("alt").replace("Edit", ""));
      // Converti JSON nel formato corretto per gli elementi da modificare
      var per = JSON.parse(tblPersons[selected_index]); 
      $("#Nome").val(per.Nome);
      $("#CF").val(per.CF);
      $("#Telefono").val(per.Telefono);
      $("#Email").val(per.Email);
      $("#Comune").val(per.Comune);
      $("#Regione").val(per.Regione );
    
    });
  
    $(".btnDelete").bind("click", function () {
      // Ottieni l'identificatore dell'elemento da eliminare
      selected_index = parseInt($(this).attr("alt").replace("Delete", "")); 
      Delete(); // Eliminazione del record
      List(); // Torna agli elementi elencati nella tabella
    });
  });